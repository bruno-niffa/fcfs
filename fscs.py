#!/usr/bin/env python
# FCFC(First-Come, First Served)
# "Utiliza um algoritmo simples que atende as tarefas em sequência assim que ficam prontas. Ou seja, de acordo com sua chegada na fila de prontos (FIFO). Não utiliza preempção (interrupção de tarefas)"

process_queue = []
total_wtime = 0
n = int(input('Enter the total no of processes: '))
for i in range(n):
    process_queue.append([])  # append a list object to the list
    process_queue[i].append(input('Enter process_name: '))
    process_queue[i].append(int(input('Enter process_arrival: ')))
    total_wtime += process_queue[i][1]
    process_queue[i].append(int(input('Enter process_burst: ')))
    print('')

process_queue.sort(key=lambda process_queue: process_queue[1])

print
'ProcessName\tArrivalTime\tBurstTime'
for i in range(n):
    print
    process_queue[i][0], '\t\t', process_queue[i][1], '\t\t', process_queue[i][2]

print ('Total waiting time: ', total_wtime)
print ('Average waiting time: ', (total_wtime / n))